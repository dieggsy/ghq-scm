(declare (unit ghq-utils))
(use s
     (prefix ansi-escape-sequences ansi:)
     (prefix alist-lib al-lib:)
     posix
     utils)

(: print-cmd (string -> undefined))
(define (print-cmd cmd)
  (if (string? cmd)
      (let* ((components (s-split " " cmd))
             (cmd (car components))
             (sub-cmd (if (> (length components) 2)
                          (cadr components)
                          #f)))
        (if (string= cmd "sudo")
            (printf "~A ~A ~A~%~!"
                    (ansi:set-text '(fg-green) cmd)
                    (ansi:set-text '(fg-green bold) sub-cmd)
                    (s-join " " (cddr components)))
            (printf "~A ~A~%~!"
                    (ansi:set-text '(fg-green bold) cmd)
                    (s-join " " (cdr components)))))
      (printf "~S~%~!" cmd)))

(: run ((or list string) boolean -> undefined))
(define (run cmds #!key verbose)
  (let* ((cmds (if (string? cmds) (list cmds) cmds)))
    (map
	 (lambda (cmd)
       (unless (or (eq? verbose 'silent)
                   (eq? verbose 'stdout))
	     (print-cmd cmd))
       (if (string? cmd)
	       (let ((r (if (or (not verbose) (eq? verbose 'silent))
                        (system (format "{ ~A ; } > /dev/null 2>&1" cmd))
                        (system cmd))))
		     (when (not (zero? r))
		       (error
			    'execute
			    "shell command failed with non-zero exit status"
			    cmd r)))
           (eval cmd)))
	 cmds)
    (void)))

(: capture (string -> string))
(define (capture cmd)
  (with-input-from-pipe cmd read-all))

(: shell-expand (string -> string))
(define (shell-expand str)
  (capture (format "echo -n ~A" str)))

(: path-expand-and-compare (string string -> boolean))
(define (path-repo-compare path repo)
  (string= path (repo->dir repo)))

(: parse-repo (string -> string))
(define (parse-repo repo)
  (if (not (or (string-prefix? "https://" repo)
               (string-prefix? "git://" repo)))
      (if (not (string-contains repo "/"))
          (string-append "https://github.com/" repo "/" repo)
          (string-append "https://github.com/" repo))
      repo))

(: ghq-conf-file string)
(define ghq-conf-file (shell-expand "~/.ghq-build.scm"))

(: ghq-load-conf (-> list))
(define (ghq-load-conf) (read-file ghq-conf-file))

(: ghq-root-dir string)
(define ghq-root-dir
  (let ((env (get-environment-variable "GHQ_ROOT")))
    (shell-expand (or env "~/.ghq/"))))

(: repo->dir (string -> string))
(define (repo->dir repo)
  (create-directory
   (string-append
    ghq-root-dir
    (string-join
     (cdr
      (string-split
       (s-chop-suffix ".git" (parse-repo repo))
       "/"))
     "/"))
   #t))

(: ghq-dirs (-> list))
(define (ghq-dirs)
  (append
   (sort
    (map (lambda (str)
           (s-chop-suffix "/.git" str))
         (find-files
          ghq-root-dir
          test: (lambda (str) (s-ends-with? "/.git" str))
          limit: (lambda (dir)
                   (not
                    (or (s-ends-with? "/.git" dir)
                        (directory?
                         (string-append
                          (pathname-directory dir)
                          "/.git")))))
          dotfiles: #t))
    string<)
   (sort
    (let* ((conf-repos (alist-ref 'repos (ghq-load-conf)))
           (repo-specs (al-lib:alist-values conf-repos)))
      (map shell-expand
           (filter identity
                   (map (lambda (alst) (alist-ref 'directory alst))
                        repo-specs))))
    string<)))

(: ghq-subcmds list)
(define ghq-subcmds '("build" "root" "look" "get" "update" "import" "list"))

(: get-index ((* -> boolean) list -> (or fixnum false) *))
(define (get-index fn lst)
  (let loop ((lst lst)
             (ind 0))
    (let ((elem (car lst))
          (rest (cdr lst)))
      (cond ((fn elem)
             (values ind elem))
            ((not (null? rest))
             (loop rest (+ ind 1)))
            (else (values #f #f))))))

(: ghq-getopt (list -> (list (or string false) (or list false))))
(define (ghq-getopt args)
  (let-values (((ind cmd) (get-index (lambda (str) (member str ghq-subcmds)) args)))
    (if ind
        (let* ((base-args (take args ind))
               (base-args (if ind (if (null? base-args) #f base-args) args))
               (sub-args (drop args (+ 1 ind)))
               (sub-args (if ind sub-args #f)))
          (values base-args cmd sub-args))
        (values args #f #f))))

(: pretty-print-conf (list -> void))
(define (pretty-print-conf lst)
  (let ((pretty-print-width 80))
    (let loop ((lst lst))
      (pretty-print (car lst))
      (when (> (length lst) 1)
        (printf "~%")
        (loop (cdr lst))))))
