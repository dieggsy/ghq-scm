ghq: ghq-utils.scm ghq.scm
	csc -optimize-level 3 ghq.scm ghq-utils.scm -o ghq

install:
	cp ghq /usr/local/bin/ghq

clean:
	rm ghq
