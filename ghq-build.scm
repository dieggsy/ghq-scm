;; (conf
;;  (look-filter . "peco")
;;  )

(look-fiter . "peco")


(repos
 ((iosevka . "be5invis/iosevka")
  (commands
   "make clean"
   "make custom-config set=term design='term v-asterisk-low' italic='v-i-serifed v-l-serifed v-a-doublestorey v-g-doublestorey'"
   "make custom set=term"
   "rm -rf ~/.local/share/fonts/iosevka-term/"
   "mv dist/iosevka-term/ttf ~/.local/share/fonts/iosevka-term"
   "fc-cache -f -v")
  (time . "2017-08-26T08:41:57"))

 ((emacs . "git://git.savannah.gnu.org/emacs")
  (commands
   "./autogen.sh"
   "./autogen.sh git"
   "./configure"
   "make"
   "sudo make install")
  (time . "2017-08-26T13:32:39"))

 ((sbcl . "git://git.code.sf.net/p/sbcl/sbcl")
  (commands
   "./make.sh --with-sb-core-compression"
   "INSTALL_ROOT=/usr/local sudo ./install.sh")
  (time . "2017-08-24T17:20:49"))

 ((rofi . "DaveDavenport/rofi")
  (commands
   "git submodule update --init"
   "autoreconf -i"
   "mkdir -p build"
   (change-directory "build")
   "../configure"
   "make"
   "sudo make install")
  (time . "2017-08-23T14:46:28"))

 ((i3 . "airblader/i3-gaps")
  (commands
   "autoreconf --force --install"
   "rm -rf build/"
   "mkdir -p build"
   (change-directory "build")
   "../configure --prefix=/usr --sysconfdir=/etc --disable-sanitizers"
   "make"
   "sudo make install")
  (time . "2017-08-21T15:55:30"))

 ((zsh . "git://git.code.sf.net/p/zsh/code")
  (commands
   "./Util/preconfig"
   "./configure --prefix=/usr/local"
   "make"
   "make check"
   "sudo make install"
   "sudo make install.info")
  (time . "2017-08-25T15:03:55"))

 ((tmux . "tmux/tmux")
  (commands "./autogen.sh" "./configure" "make" "sudo make install")
  (time . "2017-08-24T09:10:36"))

 ((chicken . "git://code.call-cc.org/chicken-core")
  (commands
   "make PLATFORM=linux spotless"
   "make PLATFORM=linux"
   "sudo make PLATFORM=linux install")
  (time . "2017-08-21T09:48:03"))

 ((ripgrep . "BurntSushi/ripgrep")
  (commands
   "cargo build --release"
   "sudo cp target/release/rg /usr/local/bin/rg"
   "sudo cp doc/rg.1 /usr/local/man/man1/rg.1")
  (time . "2017-08-26T11:20:51"))

 ((exa . "ogham/exa")
  (commands "make" "sudo make install")
  (time . "2017-08-26T11:21:34"))

 ((hub . "github/hub")
  (commands "sudo make install prefix=/usr/local")
  (time . "2017-08-26T11:34:47"))

 ((peco . "peco")
  (commands "glide install" "go build cmd/peco/peco.go")
  (directory . "~/go/src/github.com/peco/peco")
  (time . "2017-08-26T12:39:19")))
