(declare (uses ghq-utils))
(use getopt-long
     extras
     srfi-13
     (prefix alist-lib al-lib:)
     posix
     utils
     s
     srfi-19
     irregex
     (prefix ansi-escape-sequences ansi:))

(: options list)
(define options
  '((verbose (single-char #\v))
    (help (single-char #\h))))

(: help-or-error (-> void))
(define (help-or-error)
  (printf "NAME:
   ghq - Manage GitHub repository clones

USAGE:
   ghq [global options] command [command options] [arguments...]

VERSION:
   0.1.0

AUTHOR:
   dieggsy <dieggsy@protonmail.com>

COMMANDS:
     get      Clone/sync with a remote repository
     list     List local repositories
     look     Look into a local repository
     import   Bulk get repositories from stdin
     root     Show repositories' root
     help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h     show help
   --version, -v  print the version~%"))

(: ghq-get (#!rest string -> void))
(define (ghq-get . rest)
  (define options
    '((verbose (single-char #\v))
      (help (single-char #\h))
      (directory (value (required DIR))
                 (single-char #\d))))
  (let* ((args (getopt-long rest options))
         (verbose (alist-ref 'verbose args))
         (opt-dir (alist-ref 'directory args))
         (args (alist-ref '@ args))
         (repo (car args))
         (symbol-repo (string->symbol repo))
         (conf (ghq-load-conf))
         (conf-repos (alist-ref 'repos conf))
         (keys (al-lib:alist-keys conf-repos))
         (conf-repo (alist-ref symbol-repo keys))
         (alternate-dir (when conf-repo
                          (alist-ref
                           'directory
                           (alist-ref (cons symbol-repo conf-repo) conf-repos equal?)))))
    (let ((full-repo (if conf-repo (parse-repo conf-repo)
                         (parse-repo repo)))
          (dir (or opt-dir
                   (and alternate-dir (shell-expand alternate-dir))
                   (if conf-repo (repo->dir conf-repo)
                       (repo->dir repo)))))
      (when opt-dir
        (create-directory (pathname-directory opt-dir) #t))
      (if (not (and (directory? dir) (member ".git" (directory dir #t))))
          (run (format "git clone ~A ~A" full-repo dir) verbose: verbose)
          (printf "It seems you already have that repo.~%")))))

(: ghq-import (#!rest string -> void))
(define (ghq-import . rest) (void))


(: ghq-root (#!rest string -> void))
(define (ghq-root . rest)
  (printf "~A~%" ghq-root-dir))

(: ghq-build (#!rest string -> void))
(define (ghq-build . rest)
  (define options
    '((all)
      (verbose (single-char #\v))
      (help (single-char #\h))))
  (let* ((args (getopt-long rest options))
         (verbose (alist-ref 'verbose args))
         (all (alist-ref 'all args))
         (args (alist-ref '@ args))
         (conf (ghq-load-conf))
         (conf-repos (alist-ref 'repos conf))
         (names (if (null? args) #f (map string->symbol args)))
         (cwd (current-directory))
         (keys (al-lib:alist-keys conf-repos)))
    (define (build key)
      (let* ((entry (alist-ref key conf-repos equal?))
             (last-time (or (and entry (alist-ref 'time entry))
                            (format-date "~Y-~m-~dT~H:~M:~S" (seconds->date 0)))))
        (if entry
            (begin
              (when (or names all)
                (let* ((conf-dir (alist-ref 'directory entry))
                       (dir (or (and conf-dir (shell-expand conf-dir))
                                (repo->dir (cdr key)))))
                  (print-cmd `(change-directory ,dir))
                  (change-directory dir)))
              (run "git pull" verbose: verbose)
              (if (not (string-null?
                        (capture (format "git --no-pager log --date=local --after='~A'"
                                         last-time))))
                  (begin
                    (run (alist-ref 'commands entry) verbose: verbose)
                    (with-output-to-file ghq-conf-file
                      (lambda ()
                        (pretty-print-conf
                         (alist-update
                          'repos
                          (alist-update
                           key
                           (alist-update 'time
                                         (format-date "~Y-~m-~dT~H:~M:~S" (time->date (current-time)))
                                         entry)
                           conf-repos
                           equal?)
                          conf)))))
                  (printf "~A: Already up to date.~%"
                          (ansi:set-text '(fg-white) "Build"))))
            (printf "Nothing to build.~%"))))
    (cond (all
           (let loop ((keys keys))
             (when (> (length keys) 0)
               (printf "~A ~A~%" (ansi:set-text '(fg-white) "Building") (caar keys))
               (build (car keys))
               (printf "~%")
               (loop (cdr keys)))))
          (names
           (let loop ((names names))
             (when (> (length names) 0)
               (let ((key (assoc (car names) keys)))
                 (when key
                   (printf "~A ~A~%" (ansi:set-text '(fg-white) "Building") (car key)))
                 (build key)
                 (when (> (length names) 1)
                   (printf "~%"))
                 (loop (cdr names))))))
          (else
           (let ((key (rassoc cwd keys path-repo-compare)))
             (build key))))
    (exit 0)))

(: ghq-update (#!rest string -> void))
(define (ghq-update . rest)
  (define options
    '((verbose (single-char #\v))
      (help (single-char #\h))))
  (let* ((args (getopt-long rest options))
         (verbose (if (alist-ref 'verbose args) 'stdout 'silent))
         (dirs (ghq-dirs))
         (short-dirs (map (lambda (str) (s-chop-prefix ghq-root-dir str))
                          dirs)))
    (let loop ((dirs dirs))
      (when (> (length dirs) 0)
        (let ((dir (car dirs)))
          (change-directory dir)
          (printf "~A ~A"
                  (ansi:set-text '(fg-white) "pull")
                  (string-pad-right
                   (s-chop-prefix ghq-root-dir dir)
                   (+ 4
                      (apply max
                             (map (lambda (dir) (string-length dir))
                                  short-dirs)))))
          (let ((num-commits
                 (length
                  (string-split
                   (capture "git fetch > /dev/null 2>&1 && git --no-pager log ..'@{u}' --pretty=format:%h")
                   "\n"))))
            (if (> num-commits 0)
                (begin
                  (when (and verbose (not (eq? verbose 'silent)))
                    (printf "~%"))
                  (run "git pull" verbose: verbose)
                  (printf "~A (~A commits).~%"
                          (ansi:set-text '(fg-green bold) "updated")
                          num-commits))
                (printf (ansi:set-text '(fg-white) "up-to-date~%")))))
        (loop (cdr dirs))))
    (exit 0)))

(: ghq-list (#!rest string -> void))
(define (ghq-list . rest)
  (let* ((home (shell-expand "$HOME"))
         (dirs (map (lambda (str) (string-append "~" (s-chop-prefix home str)))
                    (ghq-dirs))))
    (printf "~A~%" (string-join dirs "\n"))))

(: ghq-look (#!rest string -> void))
(define (ghq-look . rest)
  (let* ((conf (ghq-load-conf))
         (look-filter (alist-ref 'look-filter conf))
         ;; (conf-repos (alist-ref 'repos conf))
         ;; (search (car rest))
         ;; (keys (al-lib:alist-keys conf-repos))
         ;; (key (assoc (string->symbol search) keys))
         )
    (cond (look-filter
           (let-values (((inp out _) (process "peco")))
             (change-directory
              (with-output-to-string
                (lambda ()
                  (with-output-to-pipe look-filter
                    ghq-list)))
              )
             )
           (system "$SHELL -i")))
    ;; (if key
    ;;     (begin
    ;;       ()
    ;;       (change-directory (repo->dir (cdr key)))))
    ))

(: main (-> void))
(define (main)
  (let-values (((base-args cmd cmd-args) (ghq-getopt (command-line-arguments))))
    (when base-args
      (let* ((args (getopt-long base-args options))
             (help? (alist-ref 'help args)))
        (when help?
          (help-or-error)
          (exit 0))))
    (when cmd
      (let ((fn (string->symbol (string-append "ghq-" cmd))))
        (apply (eval fn) cmd-args)))
    (exit 0)))

(main)
